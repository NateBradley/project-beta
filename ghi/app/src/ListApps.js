import React from 'react';
class ListApp extends React.Component {
    constructor(props) {
        super(props)
        this.state = {
            Apps: [],
        };
        this.handleOnCanceled = this.handleOnCanceled.bind(this)
        this.handleOnFinished = this.handleOnFinished.bind(this)
    }
    async handleOnCanceled(event) {
        const AppUrl = `http://localhost:8080/api/appdetails/${event}/`
        const fetchConfig = {
            method: 'put',
            body: JSON.stringify({ is_canceled: true }),
            headers: {
                'Content-Type': 'application/json'
            },
        };
        const response = await fetch(AppUrl, fetchConfig)
        if (response.ok) {
            const response2 = await fetch(`http://localhost:8080/api/apps/`)
            if (response2.ok) {
                const data = await response2.json()
                this.setState({ 'Apps': data.Apps })
            }
        }
    }
    async handleOnFinished(event) {
        const AppUrl = `http://localhost:8080/api/appdetails/${event}/`
        const fetchConfig = {
            method: 'put',
            body: JSON.stringify({ is_finished: true }),
            headers: {
                'Content-Type': 'application/json'
            },
        };
        const response = await fetch(AppUrl, fetchConfig)
        if (response.ok) {
            const response2 = await fetch('http://localhost:8080/api/apps/')
            if (response2.ok) {
                const data = await response2.json()
                this.setState({ 'Apps': data.Apps })
            }
        }
    }
    async componentDidMount() {
        const appUrl = "http://localhost:8080/api/apps/";
        const appsResponse = await fetch(appUrl)
        if (appsResponse.ok) {
            const data = await appsResponse.json()
            this.setState({ 'Apps': data.Apps })
        } else {
            throw new Error('response is not ok')
        }
    }
    render() {
        return (
            <div className="container-fluid p-4 my-1">
                <h1> Service Appointments</h1>
                <p>  </p>
                <h3> Vip Appointments</h3>
                <table className="table table-striped">
                    <thead>
                        <tr>
                            <th scope="col">VIN</th>
                            <th scope="col">Customer Name</th>
                            <th scope="col">Date</th>
                            <th scope="col">Time</th>
                            <th scope="col">Technician</th>
                            <th scope="col">Reason</th>
                        </tr>
                    </thead>
                    <tbody>
                        {this.state.Apps.map((app) => {
                            if (!app.is_finished && app.vip && !app.is_canceled) {
                                return (
                                    <tr key={app.id}>
                                        <td>{app.vin}</td>
                                        <td>{app.owner}</td>
                                        <td>
                                            {new Date(app.date).toLocaleDateString(
                                                "en-US"
                                            )
                                            }
                                        </td>
                                        <td>
                                            {new Date(app.date).toLocaleTimeString(
                                                "en-US"
                                            )}
                                        </td>
                                        <td>{app.tech.name}</td>
                                        <td>{app.reason}</td>
                                        <td>
                                            <button className="btn btn-danger" onClick={(e) => this.handleOnCanceled(app.id)}>Cancel</button>
                                            <button className="btn btn-success" onClick={(e) => this.handleOnFinished(app.id)}>Finished</button>
                                        </td>
                                    </tr>
                                );
                            }
                        })}
                    </tbody>
                </table>
                <p>  </p>
                <h4> Non Vip Appointments</h4>
                <table className="table table-striped">
                    <thead>
                        <tr>
                            <th scope="col">VIN</th>
                            <th scope="col">Customer Name</th>
                            <th scope="col">Date</th>
                            <th scope="col">Time</th>
                            <th scope="col">Technician</th>
                            <th scope="col">Reason</th>
                        </tr>
                    </thead>
                    <tbody>
                        {this.state.Apps.map((app) => {
                            if (!app.is_finished && !app.vip && !app.is_canceled) {
                                return (
                                    <tr key={app.id}>
                                        <td>{app.vin}</td>
                                        <td>{app.owner}</td>
                                        <td>
                                            {new Date(app.date).toLocaleDateString(
                                                "en-US"
                                            )
                                            }
                                        </td>
                                        <td>
                                            {new Date(app.date).toLocaleTimeString(
                                                "en-US"
                                            )}
                                        </td>
                                        <td>{app.tech.name}</td>
                                        <td>{app.reason}</td>
                                        <td>
                                            <button className="btn btn-danger" onClick={(e) => this.handleOnCanceled(app.id)}>Cancel</button>
                                            <button className="btn btn-success" onClick={(e) => this.handleOnFinished(app.id)}>Finished</button>
                                        </td>
                                    </tr>
                                );
                            }
                        })}
                    </tbody>
                </table>
            </div>
        )
    }
}

export default ListApp;
