import React from 'react';
import { NavLink } from 'react-router-dom'
class CreateVehicle extends React.Component {
    constructor(props) {
        super(props)
        this.state = {
            name: "",
            picture_url: "",
            manufacturer_id: '',
            manufacturers: [],
            hasVmCreated: false,
        };
        this.handleInputChange = this.handleInputChange.bind(this)
        this.handleSubmit = this.handleSubmit.bind(this)
    }
    handleInputChange(event) {
        const name = event.target.name
        const value = event.target.value
        this.setState({
            [name]: value
        });
    }
    async handleSubmit(event) {
        event.preventDefault();
        const data = { ...this.state }
        delete data.manufacturers
        delete data.hasVmCreated
        const VmUrl = 'http://localhost:8100/api/models/'
        const datajson = JSON.stringify(data)
        const fetchConfig = {
            method: "post",
            body: datajson,
            headers: {
                'Content-Type': 'application/json'
            },
        };
        const response = await fetch(VmUrl, fetchConfig)
        if (response.ok) {
            const newVM = await response.json()
            const cleared = {
                manufacturer_id: "",
                name: "",
                picture_url: "",
                hasVmCreated: true,
            };
            this.setState(cleared)
        } else {
            throw new Error('response is not ok')
        }
    }
    async componentDidMount() {
        const ManUrl = 'http://localhost:8100/api/manufacturers/'
        const manResponse = await fetch(ManUrl)
        if (manResponse.ok) {
            const data = await manResponse.json()
            this.setState({ 'manufacturers': data.manufacturers })
        } else {
            throw new Error('response is not ok')
        }
    }
    render() {
        let successAlert = "alert alert-success d-none mb-0"
        let formClass = '';
        if (this.state.hasVmCreated) {
            successAlert = "alert alert-success mb-0"
            formClass = 'd-none';
        }
        return (
            <div className="row">
                <div className="offset-3 col-6">
                    <div className="shadow p-4 mt-4">
                        <h1>Create A New Vehicle</h1>
                        <form
                            className={formClass}
                            onSubmit={this.handleSubmit}
                            id="create-vehicle-form">
                            <div className="form-floating mb-3">
                                <input
                                    onChange={this.handleInputChange}
                                    placeholder="Name"
                                    required type="text"
                                    id="name"
                                    name="name"
                                    className="form-control"
                                    value={this.state.name} />
                                <label htmlFor="name">Name</label>
                            </div>
                            <div className="form-floating mb-3">
                                <input
                                    onChange={this.handleInputChange}
                                    placeholder="picture_url"
                                    required type="text"
                                    name="picture_url"
                                    id="picture_url"
                                    className="form-control"
                                    value={this.state.picture_url} />
                                <label htmlFor="style_name">Picture Url</label>
                            </div>
                            <div className="mb-3">
                                <select
                                    onChange={this.handleInputChange}
                                    name="manufacturer_id"
                                    id="manufacturer_id"
                                    className="form-select"
                                    value={this.state.manufacturer_id}
                                    required>
                                    <option value="">Choose a Manufacturer</option>
                                    {this.state.manufacturers.map(manufacturer => {
                                        return (
                                            <option key={manufacturer.id} value={manufacturer.id}>
                                                {manufacturer.name}
                                            </option>
                                        )
                                    })};
                                </select>
                            </div>
                            <button className="btn btn-primary">Create</button>
                        </form>
                        <div
                            className={successAlert}
                            id="success-message">
                            Congratulations! You Created a Vehicle!
                            <div>
                                <button className="btn-block">
                                    <NavLink className="nav-link" to="/vehicles/list/">Go To List of Vehicles</NavLink>
                                </button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        )
    }
}

export default CreateVehicle;
