import React from 'react';


class SalesRecordDetail extends React.Component {
    constructor(props) {
        super(props)
        this.state = {
            sales_records: [],
            sales_people: [],
            sales_person: '',
            records_id: [],

        }
        this.handleChangeSalesPerson = this.handleChangeSalesPerson.bind(this);
    }


    async componentDidMount() {
        const sales_peopleurl = "http://localhost:8090/api/salespeople/";
        const salespeopleresponse = await fetch(sales_peopleurl);

        if (salespeopleresponse.ok) {
            const salespeopledata = await salespeopleresponse.json();
            this.setState({ sales_people: salespeopledata.sales_person });
        }
    }



    async handleChangeSalesPerson(event) {
        const value = event.target.value;
        this.setState({ sales_person: value })
        const salesUrl = `http://localhost:8090/api/salesrecords/`;
        const salesResponse = await fetch(salesUrl);

        if (salesResponse.ok) {
            const salesData = await salesResponse.json();
            this.setState({sales_records: salesData.sales_records})
        }
    }

    render() {
        return (
            <>
                <div className="px-4 py-5 my-5 mt-0 text-center">
                    <img className="bg-white rounded shadow d-block mx-auto mb-4" alt="" width="600" />
                    <h1 className="display-5 fw-bold">Sales Records</h1>
                </div>

                <div className="mb-3">
                    <select onChange={this.handleChangeSalesPerson} value={this.state.sales_person} required name="sales_person" id="sales_person" className="form-select">
                        <option value="">Choose a Sales Employee</option>
                        {this.state.sales_people.map(sales_person => {
                            return (
                                <option key={sales_person.id} value={sales_person.id}>{sales_person.name}</option>
                            )
                        })}
                    </select>
                </div>
                <table className="table table-bordered ">
                    <thead>
                        <tr>
                            <th>Sales Employee</th>
                            <th>Customer</th>
                            <th>Vin</th>
                            <th>Sale Price</th>
                        </tr>
                    </thead>
                    <tbody>
                        {this.state.sales_records.filter(record => record.sales_person.id == this.state.sales_person).map(record => {
                            return (
                                <tr className="w-25 p-3" key={record.id}>
                                    <td> {record.sales_person.name} </td>
                                    <td> {record.customer.name} </td>
                                    <td> {record.automobile.vin} </td>
                                    <td> {record.price} </td>
                                </tr>
                            )
                        })}
                    </tbody>
                </table>
            </>
        )
    }
}

export default SalesRecordDetail
