import React from 'react'
import { NavLink } from 'react-router-dom'

class ManufacturerList extends React.Component {
    constructor(props){
        super(props)
        this.state = {
            "manufacturers": []
        }
    }

    async componentDidMount() {
        const url = "http://localhost:8100/api/manufacturers/"
        let response = await fetch(url)

        if (response.ok) {
            let data = await response.json()
            this.setState({ "manufacturers": data.manufacturers })
        }
    }

    render() {
        return (
            <div className="container-fluid p-4 my-1">
                <h1> Manufacturers </h1>
                <table className="table table-bordered ">
                    <thead>
                        <tr>
                            <th>Manufacturer Name</th>
                        </tr>
                    </thead>
                    <tbody>
                        {this.state.manufacturers.map((manufacturer, id) => {
                            return (
                                <tr className="w-25 p-3" key={id}>
                                    <td> {manufacturer.name} </td>
                                </tr>
                            )
                        })}
                    </tbody>
                </table>
            </div>
        )
    }
}

export default ManufacturerList
