# CarCar

Team:

- Craig Celestin - Service

- Nate Bradley - Sales

# Design

CarCar is a an app that allows users to manage the aspects of an automobile dealership: inventory, services, and sales.

This application utilized DDD (Domain Driven Design) in order to provide a solution to the problem domain using three bounded contexts. These bounded contexts are mapped to three microservices: inventory, automboile services, and automobile sales. Below is an application diagram showcasing bounded contexts and model relationships.

The Backend Framework uses Django and Python, while the frontend was developed using React.

## Application Diagram

![](images/Project-beta.png)

# Getting Started
Perform these steps in order to start the application:

1. Fork and clone from the repository at https://gitlab.com/NateBradley/project-beta
2. Press the blue Clone button with the dropdown arrow and either copy the url or press the copy button next to the 'Clone with HTTPS' option
3. Open VSCode Terminal/a Seperate Terminal and navigate to it. There are two options: (1) navigate to the desired directory (cd <'desired directory'>) or (2) make a directory (mkdir <'desired directory'>) that the app will be stored in and naviate to it (cd <'desired directory'>).
4. Once navigated to the correct directory, execute git clone with the copied repo link i.e git clone https://gitlab.com/NateBradley/project-beta.git.
5. Type in code . to open the app in a vscode window.
6. This app requires Docker - Please perform the following steps:


    1. Open Docker Desktop

    2. Then execute:

        a. docker volume create beta-data

        b. docker-compose build

        c. docker-compose up
    3. View app by navigating to http://localhost:3000/

// When running docker-compose up (if on macOS), there will be a warning regarding the OS environment variable missing. This can be safely ignored //

**Reccomended Starting Steps**

1. Create a Manufacturer
2. Create a Vehicle with said Manufacturer
3. Add an Automobile with the created Vehicle Model
4. Delve into the Sales or Service Microservice

# Sales Microservice

The Sales microservice was developed using RESTful methods that provide frontend capabilities of managing employees, customers, sales records, and employee sales records.

Models within the sales microservice includes
* SalesPerson : name and employee number properties
* Customer : name, address, and phone number properties
* SalesRecord : automobile, sales person, customer, and price properties
* AutomobileVO : vin, import href, and has sold properties

| Goal | Method | URL |
| ----------- | ----------- | ------------|
| List Sales People | GET | http://localhost:8090/api/salespeople/
| Detail Sales Person | GET |http://localhost:8090/api/salespeople/<<int:id>>/
| Create Sales Person | POST | http://localhost:8090/api/salespeople/
| Edit Sales Person | PUT | http://localhost:8090/api/salespeople/<<int:id>>/
| Delete Sales Person | DELETE |http://localhost:8090/api/salespeople/<<int:id>>/
| List of Customers | GET | http://localhost:8090/api/customers/
| Detail Customer | GET | http://localhost:8090/api/customers/<<int:id>>
| Create a Customer | POST | http://localhost:8090/api/customers/
| Edit a Customer | PUT | http://localhost:8090/api/customers/<<int:id>>/
| Delete a Customer | DELETE | http://localhost:8090/api/customers/<<int:id>>/
| List of Sales Records | GET | http://localhost:8090/api/salesrecords/
| Detail of Sales Record | GET | http://localhost:8090/api/salesrecords/<<int:id>>/
| Create a Sales Record | POST | http://localhost:8090/api/salesrecords/
| Edit a Sales Record | PUT | http://localhost:8090/api/salesrecords/<<int:id>>/
| Delete a Sales Record | DELETE | http://localhost:8090/api/salesrecords/<<int:id>>/
| List of all Automobiles | GET | http://localhost:8100/api/automobiles/
| List of unsold Automobiles | GET | http://localhost:8090/api/automobiles/

*some methods (PUT, DELETE) are not available on the front end*

### List sales people (GET)
**ex. response**
```
{
	"sales_person": [
		{
			"name": "Lewis",
			"employee_number": 44,
			"id": 2
		},
		{
			"name": "Noris",
			"employee_number": 4,
			"id": 3
		},
    ]
}
```

### Create Sales Person (POST)
**ex. request**
```
{
	"name": "John",
	"employee_number": "100"
}
```
**ex. response**
```
{
	"name": "John",
	"employee_number": "100",
	"id": 5
}
```
### Get detail Sales Person (GET)
**ex. response**
```
{
	"name": "Lewis",
	"employee_number": 44,
	"id": 2
}
```


### Delete Sales Person (DELETE)
**ex. reponse**
```
{
	"message": "Sales person deleted"
}
```

### List of Customers (GET)
**ex. reponse**
```
{
	"customers": [
		{
			"name": "customer 1",
			"address": "111 one lane, London, UK",
			"phone_number": "123-456-7789",
			"id": 1
		},
		{
			"name": "customer 2",
			"address": "2nd st Silverstone, UK",
			"phone_number": "222-222-2222",
			"id": 2
		},
    ]
}
```
### Create a Customer (POST)
**ex. request**
```
{
	"name": "customer 3",
	"address": "Third ave, Naples, FL, USA",
	"phone_number": "888-880-8080"
}
```
**ex. response**
```
{
	"name": "customer 3",
	"address": "Third ave, Naples, FL, USA",
	"phone_number": "888-880-8080",
	"id": 3
}
```

### Detail Customer (GET)
**ex. response**
```
{
	"name": "customer 2",
	"address": "2 st Silverstone, UK",
	"phone_number": "123-456-7989",
	"id": 2
}
```

### Delete a Customer (DELETE)
**ex. response**
```
{
	"message": "Customer Deleted"
}
```

### List of Sales Records (GET)
**ex. response**
```
{
	"sales_records": [
		{
			"automobile": {
				"vin": "1C3CC5FB2AN120174",
				"import_href": "/api/automobiles/1C3CC5FB2AN120174/"
			},
			"sales_person": {
				"name": "Lewis",
				"employee_number": 44,
				"id": 2
			},
			"customer": {
				"name": "customer 1",
				"address": "111 one lane, London, UK",
				"phone_number": "123-456-7789",
				"id": 1
			},
			"price": 4000,
			"id": 5
		},
		{
			"automobile": {
				"vin": "1C3CC5FB2AN120175",
				"import_href": "/api/automobiles/1C3CC5FB2AN120175/"
			},
			"sales_person": {
				"name": "Lewis",
				"employee_number": 44,
				"id": 2
			},
			"customer": {
				"name": "customer 2",
				"address": "2nd st Silverstone, UK",
				"phone_number": "222-222-2222",
				"id": 2
			},
			"price": 9000,
			"id": 6
		}
	]
}
```
### Create a sale (POST)
**ex. request**
```
{
  "automobile": "1C3CC5FB2AN120175",
  "sales_person": "2",
	"customer": "2",
	"price": 9000
}
```
**ex. response**
```
{
	"automobile": {
		"vin": "1C3CC5FB2AN120175",
		"import_href": "/api/automobiles/1C3CC5FB2AN120175/"
	},
	"sales_person": {
		"name": "Lewis",
		"employee_number": 44,
		"id": 2
	},
	"customer": {
		"name": "customer 2",
		"address": "2nd st Silverstone, UK",
		"phone_number": "222-222-2222",
		"id": 2
	},
	"price": 9000,
	"id": 6
}
```
### Detail of a Sale (GET)
**ex. response**
```
{
	"automobile": {
		"vin": "1C3CC5FB2AN120174",
		"import_href": "/api/automobiles/1C3CC5FB2AN120174/"
	},
	"sales_person": {
		"name": "Senna",
		"employee_number": 1,
		"id": 1
	},
	"customer": {
		"name": "customer 2",
		"address": "2nd st Silverstone, UK",
		"phone_number": "222-222-2222",
		"id": 2
	},
	"price": 42000,
	"id": 2
}
```
### Delete a Sale (DELETE)
**ex. response**
```
{
	"message": "Sales record deleted"
}
```
### List of unsold Automobiles (GET)
**ex. response**
```
{
	"automobiles": [
		{
			"vin": "12345ASDFG12345MK",
			"import_href": "/api/automobiles/12345ASDFG12345MK/"
		}
	]
}
```

# Services Microservice

The Service Microservice allows a user to:
- Create a Technician
- View the List of Potential Technicians to perform a Service Appointment
- Create a new Appointment
- View the list of upcoming Appointments (in vip and non-vip table sections)
- Perform Cancel, Finished Operations on each Appointment
- Search for Appointments pertaining to a specific VIN and view information specific to VIN (unfinished, finished etc)

The models in the service microservice include:
- AutoVO: Representation of vehicles in inventory and obtaining all data concerning each attribute: color, year, import_href, and import_vin that are imported from the inventory api.

- Technician Model: Representation of Technician with attributes of name and employee number
- Appointment Model: Representation of Appointment with attributes of vin, date/time, reason, owner, technician, is_finished (boolean), vip(boolean), is_canceled (boolean)

The Service microservice gains data regarding automobiles via the poller from the inventory API in order to create vo objects. This information allows for assigning of a vip status: either vip (true) if the appointment created has an associated vin that matches a Automobile vin in the Inventory or vip (false) if the appointment created has no associated vin.

The Service MS is located on port 8080 and is accessed using http://localhost:8080 w/ these following endpoints

| Action | Method | URL |
| ------------- | ------------- |-------------|
| List of Automobile VOs  | GET | http://<!---->localhost:8080/api/autovos/<!---->  |
| List of Individual Automobile VO | GET  | http://<!---->gist.github.com/'str:vin'/<!----> |
| List of Technicians  | GET | http://<!---->localhost:8080/api/techs/<!---->  |
| Create a Technician | POST | http://<!---->localhost:8080/api/techs/<!---->  |
| List Appointments | GET | http://<!---->localhost:8080/api/apps/<!---->  |
| Get Appointments specific to a VIN | GET  | http://<!---->localhost:8080/api/apps/'str:vin'/<!----> |
| Create an Appointment | POST | http://<!---->localhost:8080/api/apps/<!---->  |
| Update/PUT an Appointment | PUT | http://<!---->localhost:8080/api/appdetails/'int:id'/<!----> |

**Reccomended Startup Steps**

1. Create a New Technician
2. Create an Appointment with a Desired Technician
3. Go to List Appointment Views

    a. Either mark an appointment as cancel or finished

    b. Depending on if the vin is found in Automobiles, which is connected to AutoVOs (the appointment will show up in the vip appointments or non vip appointments table)

4. Search for Appointment by VIN

## API Documentation
### Automobile VOs

- GET List Automobile VOs

    Example of Request:

    ![](images/listautos.png)

    Example of Response:

    ![](images/listautos1.png)


- GET Individual Automobile VO

    Example of Request:

    ![](images/detailvo.png)

    Example of Response:

    ![](images/detailvo1.png)


### Technicians
- GET Technician List

    Example of Request:

    ![](images/listechs.png)

    Example of Response:

    ![](images/listtechs1.png)

- POST Technician

    Example of Request:

    ![](images/posttech.png)

    Example of JSON BODY:

    ![](images/posttech1.png)

    Example of Response:

    ![](images/posttech2.png)



### Appointments
- GET List of Appointments

    Example of Request:

    ![](images/listapps.png)

    Example of Response:

    ![](images/listapps1.png)


- GET Appointments specific to a VIN

    Example of Request:

    ![](images/getapps.png)

    Example of Response:

    ![](images/getapps1.png)

- POST an Appointment

    Example of Request:

    ![](images/apps.png)

    Example of JSON Body:

    ![](images/apps2.png)

    Example of Response:

    ![](images/apps1.png)

- UPDATE (PUT) an Appointment

    Example of Request:

    ![](images/update0.png)

    (Previous Response)

    ![](images/update1.png)

    (Changes to be made - JSON BODY)

    ![](images/update2.png)

    (Response with Changes)

    Example of Response:

    ![](images/update3.png)
