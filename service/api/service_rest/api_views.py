from django.shortcuts import render
from django.http import JsonResponse
from django.views.decorators.http import require_http_methods
from .models import AutoVO, Technician, Appointment
from common.json import ModelEncoder
import json
# Create your views here.
class AutoVOListEncoder(ModelEncoder):
    model=AutoVO
    properties=[
        "import_href",
        "import_vin",
        ]

class AutoVODetailEncoder(ModelEncoder):
    model=AutoVO
    properties=[
        "import_href",
        "import_vin",
        "color",
        "year",
        ]

class TechnicianEncoder(ModelEncoder):
    model=Technician
    properties=[
        "id",
        "name",
        "employee_number"
    ]

class AppointmentEncoder(ModelEncoder):
    model=Appointment
    properties=[
        "id",
        "vin",
        "date",
        "reason",
        "owner",
        "is_finished",
        "is_canceled",
        "vip",
        "tech"
    ]
    encoders={
        "tech":TechnicianEncoder(),
    }

@require_http_methods(["GET"])
def api_list_autos(request):
    if request.method=="GET":
        Autos=AutoVO.objects.all()
        return JsonResponse(
            {"autos":Autos},
            encoder=AutoVOListEncoder,
        )

@require_http_methods(["GET"])
def api_show_autos(request, vin):
    if request.method=="GET":
        Auto=AutoVO.objects.filter(import_vin=vin)
        return JsonResponse(
            {"auto":Auto},
            encoder=AutoVODetailEncoder
        )
@require_http_methods(["GET","POST"])
def api_list_techs(request):
    if request.method=="GET":
        Techs=Technician.objects.all()
        return JsonResponse(
            {"techs":Techs},
            encoder=TechnicianEncoder
        )
    else:
        content=json.loads(request.body)
        Tech=Technician.objects.create(**content)
        return JsonResponse(
            Tech,
            encoder=TechnicianEncoder,
            safe=False
        )
@require_http_methods(['GET','POST'])
def api_list_apps(request,vin=None):
    if request.method=="GET":
        Appointments=Appointment.objects.all()
        return JsonResponse(
            {"Apps":Appointments},
            encoder=AppointmentEncoder,
        )
    elif request.method:
        content=json.loads(request.body)
        try:
            tech_id=content['tech']
            Tech=Technician.objects.get(id=tech_id)
            content['tech']=Tech
        except Technician.DoesNotExist:
            return JsonResponse(
                {'alert':'tech does not exist'},
                status=400
            )
        if(AutoVO.objects.filter(import_vin=content['vin'])):
            content['vip']=True
        else:
            content['vip']=False
        Appointments=Appointment.objects.create(**content)
        return JsonResponse(
            Appointments,
            encoder=AppointmentEncoder,
            safe=False
        )

@require_http_methods(['GET','PUT','DELETE'])
def api_show_apps(request,vin):
    if request.method=="GET":
        Appointments=Appointment.objects.filter(vin=vin)
        return JsonResponse(
            {"App":Appointments},
            encoder=AppointmentEncoder
        )

@require_http_methods(['PUT','DELETE'])
def api_show_app_details(request,id):
    if request.method=="DELETE":
       count, _=Appointment.objects.filter(id=id).delete()
       return JsonResponse({"Appointment was deleted":count>0})
    else:
        content=json.loads(request.body)
        Appointment.objects.filter(id=id).update(**content)
        Appointments=Appointment.objects.get(id=id)
        return JsonResponse(
            Appointments,
            encoder=AppointmentEncoder,
            safe=False
        )
