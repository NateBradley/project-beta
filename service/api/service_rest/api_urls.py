from django.urls import path
from .api_views import (api_list_autos,
                        api_show_autos,
                        api_list_techs,
                        api_list_apps,
                        api_show_apps,
                        api_show_app_details,
                        )

urlpatterns=[
    path("autovos/", api_list_autos, name="api_list_autos" ),
    path("autovos/<str:vin>/", api_show_autos, name="api_show_autos"),
    path("techs/", api_list_techs, name="api_list_techs"),
    path("apps/", api_list_apps, name="api_list_apps"),
    path("apps/<str:vin>/", api_show_apps, name="api_show_apps"),
    path("appdetails/<int:id>/", api_show_app_details, name="api_show_app_details")
]
